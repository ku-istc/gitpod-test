# 教師用マニュアル

## 授業用のgitリポジトリの作成

### リポジトリのfork

1. <https://gitlab.com> にアカウントがなければ，アカウントを作成する．以下アカウント名を `xxxxx` とする．
2. <https://gitlab.com> にログインする．
3. <https://gitlab.com/ku-istc/gitpod-test> を開き，右上の「Fork」ボタンを押し，自分のアカウントを選択する．
4. forkされた自分のリポジトリが開く．
5. リポジトリの設定をpublicにする．publicにしないと，学生がアクセスできない．
   - "Settings" / "General" / "Visibility, project features, permissions" の右のExpandをクリックして，
     "Project visivility"を"Public"にし，"Save Changes"ボタンをクリックする．

### Gitpodを開く

1. 自分のリポジトリの表示画面で，"Web IDE"ボタンを"Gitpod"ボタンに変更し，"Gitpod"ボタンをクリックする．
   - もしポップアップウィドウが表示されたら"Enable Gitpod"をクリックし，もう一度"Gitpod"ボタンをクリックする．
2. Gitpodのworkspaceが表示される．
3. 右上の自分のアイコンをクリックし"Open Access Control"を選ぶ．
4. GitLabの"repository access"にチェックを付けて"Update"ボタンをクリックする．

### プログラムの修正とpush

1. Gitpodでプログラムを修正する．
2. Visual Studio Codeと同様の操作で自分のGitLabリポジトリに修正をpushする．
   - 右下にpushの許可が必要というポップアップが表示されたら許可する．
